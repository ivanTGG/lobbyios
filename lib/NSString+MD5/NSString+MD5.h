//
//  NSString+MD5.h
//  Lobby
//
//  Created by Ivan Wan on 25/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (MD5)

- (NSString *)MD5;

@end
