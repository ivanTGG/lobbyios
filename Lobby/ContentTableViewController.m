//
//  contentTableViewController.m
//  Lobby
//
//  Created by Ivan Wan on 20/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "ContentTableViewController.h"

@interface ContentTableViewController ()

@property (nonatomic, assign) BOOL dataLoaded;

@end

@implementation ContentTableViewController
{
    NSArray *tableData;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Products";
    
    UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(presentLeftMenuViewController:)];
    [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = menuBtn;
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 150.0;
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self callConfigAPI];
}

- (void)callConfigAPI{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:AppConfigAPI]];
    ///NSLog(@"config:%@",configAPI);
    
    NSURLSession *session = [NSURLSession sharedSession];
    
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil) {
            //NSLog(@"data:%@",data);
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            tableData = json[@"games"];

            dispatch_async(dispatch_get_main_queue(), ^{
                [MBProgressHUD hideHUDForView:self.view animated:YES];
                [self.tableView reloadData];
            });
        }
    }];
    [dataTask resume];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *simpleTableIdentifier = @"GameTableCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    
    NSDictionary* d = [tableData objectAtIndex:indexPath.row];

    UIImageView* iv = (UIImageView*)[cell viewWithTag:10];
    UILabel* titleLbl = (UILabel*)[cell viewWithTag:11];
    UILabel* developerLbl = (UILabel*)[cell viewWithTag:12];
    UILabel* infoLbl = (UILabel*)[cell viewWithTag:13];
    UIButton* b = (UIButton*)[cell viewWithTag:20];
    
    titleLbl.text = d[@"name"];
    infoLbl.text  = d[@"description"];
    developerLbl.text = d[@"developer"];
  
    [b setTitle:d[@"link"] forState:UIControlStateNormal];
    [b addTarget:self action:@selector(cellBtnAction:) forControlEvents:UIControlEventTouchUpInside];

    iv.layer.cornerRadius = 5;    
    [iv setShowActivityIndicatorView:YES];
    [iv setIndicatorStyle:UIActivityIndicatorViewStyleGray];
    [iv sd_setImageWithURL:[NSURL URLWithString:d[@"img"]] placeholderImage:nil];
    
    return cell;
}

- (void)cellBtnAction:(id)sender {
    UIButton* b = (UIButton*)sender;
    
    NSString* url = b.titleLabel.text;
    NSURL* nsUrl = [NSURL URLWithString:url];
    UIApplication *application = [UIApplication sharedApplication];
    [application openURL:nsUrl options:@{} completionHandler:nil];
}

@end
