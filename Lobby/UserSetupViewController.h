//
//  UserSetupViewController.h
//  Lobby
//
//  Created by Ivan Wan on 25/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserSetupViewController : UIViewController <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *nameTextField;

@end
