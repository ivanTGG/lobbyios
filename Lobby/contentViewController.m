//
//  contentViewController.m
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "contentViewController.h"
#import "LandscapeWebViewController.h"

@interface contentViewController ()

@end

@implementation contentViewController
{
    NSURL *mainGamePage;
    NSURL *gameLink;
    float score;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"TGG";
    
    UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                style:UIBarButtonItemStylePlain
                                target:self
                                action:@selector(presentLeftMenuViewController:)];
    [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = menuBtn;
    
    [self.webView.scrollView setBounces:NO];
    
    [self callConfigAPI];
}

- (void)callConfigAPI{
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:AppConfigAPI]];
    ///NSLog(@"config:%@",configAPI);
    
    NSURLSession *session = [NSURLSession sharedSession];

    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        if (data != nil) {
            //NSLog(@"data:%@",data);
            
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSString* gamepage = json[@"gamepage"];
            mainGamePage = [NSURL URLWithString:gamepage];
            
            //NSLog(@"game page:%@",gamepage);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self callMainGamePageAPI];
            });
        }
    }];
    [dataTask resume];
}

- (void)callMainGamePageAPI{
 
    NSURL *nsUrl = mainGamePage;
    
    // fallback
    if (nsUrl == nil) {
        NSString *url = @"http://igaming-prototype.s3-website-ap-southeast-1.amazonaws.com/apps/lobby/";
        //    NSString *url = @"http://igaming-prototype.s3-website-ap-southeast-1.amazonaws.com/games/";
        nsUrl = [NSURL URLWithString:url];
    }
    
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    [self.webView loadRequest:request];
    
    // load user data
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
        [webView stopLoading];
        gameLink = request.mainDocumentURL;

        // banned version code
//        [webView stopLoading];
//        LandscapeWebViewController* lvc = [[LandscapeWebViewController alloc] init];
//        [self.navigationController presentViewController:lvc animated:NO completion:^{
//        }];
//        [lvc loadWebRequest:request];
        
        UIApplication *application = [UIApplication sharedApplication];
        [application openURL:gameLink options:@{} completionHandler:nil];
    }
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
