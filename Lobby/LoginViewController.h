//
//  LoginViewController.h
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : PortraitViewController

@property (strong, nonatomic) IBOutlet UIButton *loginBtn;
@property (strong, nonatomic) IBOutlet UIView *loginBtnView;

- (IBAction)tnCAction:(id)sender;
- (IBAction)signInAction:(id)sender;
- (IBAction)skipSignInAction:(id)sender;
@end
