//
//  LobbyAppConstants.h
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#ifndef LobbyAppConstants_h
#define LobbyAppConstants_h

#define AppConfigAPI                @"https://raw.githubusercontent.com/ivanwhk/TGGDevelopment/master/tgglobby.json"

#define LobbyAPPID                  1201578150
#define AppLink                     @"http://itunes.apple.com/app/id1201578150"

#define LobbyAPIpath                @"http://52.74.85.113:8888/credit-rest/rest/"
#define TeamWorkAPIpath             @"http://teamworkapi.tgginteractive.com/tgg-rest/rest/"
#define TeamWorkAPIpath_noRest      @"http://teamworkapi.tgginteractive.com/tgg-rest/"

#define slotRecordFileName          @"slotrecord"

#define LobbyAppDelegate (AppDelegate *)[[UIApplication sharedApplication] delegate]

#define  ScreenWidth    [[UIScreen mainScreen] bounds].size.width
#define  ScreenHeight   [[UIScreen mainScreen] bounds].size.height

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#endif /* LobbyAppConstants_h */
