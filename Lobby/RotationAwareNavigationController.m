//
//  RotationAwareNavigationController.m
//  Lobby
//
//  Created by Ivan Wan on 7/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "RotationAwareNavigationController.h"

@interface RotationAwareNavigationController ()

@end

@implementation RotationAwareNavigationController

-(UIInterfaceOrientationMask)supportedInterfaceOrientations{
    UIViewController *top = self.topViewController;
    return top.supportedInterfaceOrientations;
}

-(BOOL)shouldAutorotate {
    UIViewController *top = self.topViewController;
    return [top shouldAutorotate];
}

@end
