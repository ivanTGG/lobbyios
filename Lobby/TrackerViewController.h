//
//  TrackerViewController.h
//  Lobby
//
//  Created by Ivan Wan on 14/2/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICountingLabel.h"

@interface TrackerViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIView *emptyView;
@property (strong, nonatomic) IBOutlet UIView *balanceView;
@property (strong, nonatomic) IBOutlet UICountingLabel *balanceLbl;

@property (strong, nonatomic) IBOutlet UIView *titleView;

@end
