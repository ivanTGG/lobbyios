//
//  InviteViewController.m
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "InviteViewController.h"
#import <Social/Social.h>

@interface InviteViewController ()

@end

@implementation InviteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.title = @"Invite";

    UIBarButtonItem *menuBtn = [[UIBarButtonItem alloc]
                                   initWithTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FABars]]
                                   style:UIBarButtonItemStylePlain
                                   target:self
                                   action:@selector(presentLeftMenuViewController:)];
    [menuBtn setTitleTextAttributes:@{  NSFontAttributeName: [UIFont fontWithName:kFontAwesomeFamilyName size:18.0f] ,
                                        NSForegroundColorAttributeName: [UIColor whiteColor]
                                        } forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = menuBtn;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)targetedShare:(NSString *)serviceType {
    if([SLComposeViewController isAvailableForServiceType:serviceType]){
        SLComposeViewController *shareView = [SLComposeViewController composeViewControllerForServiceType:serviceType];
        
        NSString* msg = [NSString stringWithFormat:@"Hey, I recommend the TGG Lobby App to you! Check out the download link: %@",AppLink];
        
        [shareView setInitialText:msg];
        [shareView addImage:[UIImage imageNamed:@"lobby"]];
        [self presentViewController:shareView animated:YES completion:nil];

    } else {
        UIAlertView *alert;
        alert = [[UIAlertView alloc]
                 initWithTitle:@"You do not have this service. Setup this social service in your iPhone settings."
                 message:nil
                 delegate:self
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)FacebookShare:(id)sender {
    [self targetedShare:SLServiceTypeFacebook];
}

- (IBAction)TwitterShare:(id)sender {
    [self targetedShare:SLServiceTypeTwitter];
}

- (IBAction)SinaShare:(id)sender {
     [self targetedShare:SLServiceTypeSinaWeibo];
}

- (IBAction)TencentShare:(id)sender {
    [self targetedShare:SLServiceTypeTencentWeibo];
}

- (IBAction)whatsppShare:(id)sender {
    
    NSString* msg = [NSString stringWithFormat:@"Hey, I recommend the TGG Lobby App to you! Check out the download link: %@",AppLink];
    NSString* msgURL = [msg stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSString* whatsappMsg = [NSString stringWithFormat:@"whatsapp://send?text=%@",msgURL];
    NSURL *whatsappURL = [NSURL URLWithString:whatsappMsg];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL:whatsappURL options:@{} completionHandler:nil];
    }
    else {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"WhatsApp not installed." message:@"Your device has no WhatsApp installed." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
}

- (IBAction)emailShare:(id)sender {
    
    if ([MFMailComposeViewController canSendMail]) {

        NSString* msg = [NSString stringWithFormat:@"Hey, I recommend the TGG Lobby App to you! Check out the download link: %@",AppLink];
        
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setSubject:@"TGG Lobby iOS app"];
        [mailViewController setMessageBody:msg isHTML:NO];
        
        [self presentViewController:mailViewController animated:YES completion:nil];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:NSLocalizedString(@"Email account not setup on this device", @"Email account not setup on this device")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                              otherButtonTitles:nil];
        
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
