//
//  leftMenuViewController.m
//  Lobby
//
//  Created by Ivan Wan on 23/1/2017.
//  Copyright © 2017 tgg. All rights reserved.
//

#import "leftMenuViewController.h"


@interface leftMenuViewController ()

@end

@implementation leftMenuViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    NSString* nameStr = @"Guest";
    NSString* idStr = @"";
    
    UserSession* usr = [UserSession sharedInstance];
    if (usr.userId) {
        if ([UserSession sharedInstance].nickname != nil)
            nameStr = [UserSession sharedInstance].nickname;
        if ([UserSession sharedInstance].userId != nil) {
            idStr = [UserSession sharedInstance].userId;
            idStr = [NSString stringWithFormat:@"ID: %@",idStr];
        }
    }
    else {
        [self.accountSetupBkgrdBtn setHidden:YES];
        [self.accountSetupBtn setHidden:YES];
    }
    
    [self.nameLbl setText:nameStr];
    [self.idLbl setText:idStr];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.accountSetupBtn.titleLabel setFont:[UIFont fontWithName:kFontAwesomeFamilyName size:17.f]];
    [self.accountSetupBtn setTitle:[NSString stringWithFormat:@"%@", [NSString fontAwesomeIconStringForEnum:FACog]] forState:UIControlStateNormal];

    UIFont *font1 = [UIFont fontWithName:kFontAwesomeFamilyName size:23.0f];
    UIFont *font2 = [UIFont fontWithName:@"HelveticaNeue-Medium"  size:18.0f];
    NSDictionary *dict1 = @{NSFontAttributeName:font1, NSForegroundColorAttributeName : [UIColor whiteColor]};
    NSDictionary *dict2 = @{NSFontAttributeName:font2, NSForegroundColorAttributeName : [UIColor whiteColor]};
    
    NSMutableAttributedString *attString1 = [[NSMutableAttributedString alloc] init];
    [attString1 appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"    %@   ", [NSString fontAwesomeIconStringForEnum:FAGamepad]] attributes:dict1]];
    [attString1 appendAttributedString:[[NSAttributedString alloc] initWithString:@"Products"      attributes:dict2]];
    [self.btnGames setAttributedTitle:attString1 forState:UIControlStateNormal];

    NSMutableAttributedString *attString2 = [[NSMutableAttributedString alloc] init];
    [attString2 appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"    %@   ", [NSString fontAwesomeIconStringForEnum:FAUser]] attributes:dict1]];
    [attString2 appendAttributedString:[[NSAttributedString alloc] initWithString:@" Invite Users"      attributes:dict2]];
    [self.btnInvite setAttributedTitle:attString2 forState:UIControlStateNormal];
    
    NSMutableAttributedString *attString3 = [[NSMutableAttributedString alloc] init];
    [attString3 appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"    %@   ", [NSString fontAwesomeIconStringForEnum:FAnewspaperO]] attributes:dict1]];
    [attString3 appendAttributedString:[[NSAttributedString alloc] initWithString:@"Notifications"      attributes:dict2]];
    [self.btnNews setAttributedTitle:attString3 forState:UIControlStateNormal];
    
    NSMutableAttributedString *attString4 = [[NSMutableAttributedString alloc] init];
    [attString4 appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"    %@   ", [NSString fontAwesomeIconStringForEnum:FAFile]] attributes:dict1]];
    [attString4 appendAttributedString:[[NSAttributedString alloc] initWithString:@"Terms & Conditions"      attributes:dict2]];
    [self.btnTerms setAttributedTitle:attString4 forState:UIControlStateNormal];

    NSMutableAttributedString *attString0 = [[NSMutableAttributedString alloc] init];
    [attString0 appendAttributedString:[[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"    %@   ", [NSString fontAwesomeIconStringForEnum:FAcalculator]] attributes:dict1]];
    [attString0 appendAttributedString:[[NSAttributedString alloc] initWithString:@"Slot Tracker"      attributes:dict2]];
    [self.btnTracker setAttributedTitle:attString0 forState:UIControlStateNormal];
    
    self.profileIV.layer.cornerRadius = self.profileIV.frame.size.width/2;
    self.profileIV.layer.masksToBounds = YES;
    self.profileIV.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.profileIV.layer.borderWidth = 1.0;
    
    [self.btnNews setHidden:YES];
}

- (IBAction)btnTrackerAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.trackerViewController)
        appDelegate.trackerViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TrackerViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.trackerViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)btnAccountAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.accountViewController)
        appDelegate.accountViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.accountViewController animated:YES];

    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)btnGameAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.contentTableViewController)
        appDelegate.contentTableViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContentTableViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.contentTableViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}

- (IBAction)btnInviteAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.inviteViewController)
        appDelegate.inviteViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"InviteViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.inviteViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)btnNewsAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.newsViewController)
        appDelegate.newsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"NewsViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.newsViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}
- (IBAction)btnTermsAtn:(id)sender {
    AppDelegate* appDelegate = LobbyAppDelegate;
    if (!appDelegate.termsViewController)
        appDelegate.termsViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"TermsViewNavController"];
    [self.sideMenuViewController setContentViewController:appDelegate.termsViewController animated:YES];
    
    [self.sideMenuViewController hideMenuViewController];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
